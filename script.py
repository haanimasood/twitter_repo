#!/usr/bin/env python

# SYED HAANI MASOOD - TWITTER TRENDS DATA MANIPULATION

#________________________________________________________________________
#Initialization
#________________________________________________________________________
from bs4 import BeautifulSoup
import urllib
import sys
import urllib2
import json
import pprint
import time
import os
pp = pprint.PrettyPrinter(indent=4)
import datetime
now = datetime.datetime.now()
date = str(now.strftime("%Y_%m_%d-%H_%M"))
import twitter
from twitter import Twitter
import twit_funcs as twitter_funcs
import csv

#________________________________________________________________________
#Store Trend list in file with date stamp
#________________________________________________________________________
the_trends = twitter_funcs.get_trend_list()
f_trends = the_trends[0]['trends']
trend_list = []
dir = '/Users/haanimasood/Work/trend_data/'+date+'/'
twitter_funcs.ensure_dir(dir);
trend_file = open(dir+'trends.txt','w')
for each_trend in f_trends:
	trend_list.append(each_trend['name'])
	#Create file
	trend_file.write(str(each_trend['name'])+'\n')
#	pp.pprint(str(each_trend['name']))
	
#___________________________________________________________	
#____________________EXECUTION______________________________

#------------ACCOUNT LOGIN ---------------#
twit_login = 'ptibot'
twitter = twitter_funcs.authenticate(twit_login)
if twitter is not False:
	pp.pprint('Authentication passed!')
	print '\n------------------------------- \n'
else:
	pp.pprint('Authentication failed! \n')
	print '\n------------------------------- \n'
#------------ACCOUNT LOGIN ---------------#
following = {}
followers = {}
followers_list_per_user = []
followers_matrix = {}
map_user_id = 1

for x in range(0,1):
	trend_rank = x
	time_file_name = str(trend_list[trend_rank])+'TIME'+'.csv'
	user_posts_file_name = str(trend_list[trend_rank])+'POSTS'+'.csv'
	user_time_file_name = str(trend_list[trend_rank])+'USERS_TIMELINE'+'.csv'
	follow_mapping_file = 'User_mapping'+'.txt'
	follow_matrix_file  = 'Follow matrix'+'.txt'
	
	no_pages = 2
	#pp.pprint(trend_list[trend_rank])
	(distinct_users_ids, distinct_users) = twitter_funcs.execute_trend_data(trend_list[trend_rank], time_file_name, user_posts_file_name, user_time_file_name,no_pages,dir);

	for each_user in distinct_users:
		# 'True' for followers and 'False' for following
		# Get all followers of that user in a list user_followers
		user_followers = twitter_funcs.twit_follow_list(each_user,twitter,True)
		# Check if user already exists in database
		hashkey = followers.get(distinct_users_ids.get(each_user))
			
		# If user not in database, add entry in followers dictionary and add a mapped user id
		if hashkey == None:
			followers[distinct_users_ids.get(each_user)] = map_user_id
			hashkey = map_user_id
			map_user_id += 1
			
			# Loop within the followers of user and if assign mapped user id's to followers if not already exists
			for each_follower in user_followers:
				if each_follower not in followers.keys():
					followers[each_follower] = map_user_id
					map_user_id += 1
					followers_list_per_user.append(map_user_id)					
				else:
					followers_list_per_user.append(followers.get(each_follower))
		
		followers_matrix[hashkey] = followers_list_per_user
	
	# f = open(dir+str(follow_mapping_file),'w')
	# Write each user in the dictionary to a file 'f2'
	w = csv.writer(open(dir+str(follow_mapping_file),'w'))
	
	for key, val in followers.items():
		w.writerow([key, val])
    	
	#for each_user in dict.keys():
	#	f.write(str(each_dict_user)+str(dict[each_dict_user])+'\n')
	
	pp.pprint(followers_matrix)

	sys.exit(0)

"""	
	
#-------------------2--------------------------
#Extraction
req = urllib2.Request('https://api.twitter.com/1/trends/23424922.json')
response = urllib2.urlopen(req)
json_string = response.read()
the_trends = json.loads(json_string)

#Filter out relevant data
f_trends = the_trends[0]['trends']

srch = str('https://search.twitter.com/search.json?q=')+str(f_trends[0]['name']+'&rpp=100&include_entities=true&result_type=recent')
srch = srch.replace('#','%23')
srch = srch.replace(' ','%20')
print srch

#Extraction
req = urllib2.Request(srch)
response = urllib2.urlopen(req)

json_string = response.read()
publishers = json.loads(json_string)
trace = publishers['results']

dict2 = {}

#Create file
f = open('./abc2','w')


for each_user in trace:
	hashkey = dict2.get(each_user['from_user'],0)
	dict2[each_user['from_user']] = hashkey+1

#users =  sorted(dict2)
users =  dict2.keys()

for k in users:
	f.write(str(k)+','+str(dict2[k])+'\n')

b = []
	
for k in dict.keys():
	if k in dict2:
		print k
		b.append(k)

pp.pprint(len(dict))
pp.pprint(len(dict2))
pp.pprint(len(b))
		

	
	
#f.write(str(dict)+'\n')
#pp.pprint(dict)
##	f.write(str(trend['name'])+'\n')
		
#print pp.pprint(dict.values())
"""

"""import operator
sorted_users = sorted(dict.iteritems(), key=operator.itemgetter(1),reverse=True)
print pp.pprint(sorted_users)"""

"""for each_user in publishers['results']
	print each_user['from_user']"""
	
#print pp.pprint(publishers['results'])



"""
f = open('./abc','w')

for trend in f_trends:
	f.write(str(trend['name'])+'\n')
	#pp.pprint(str(trend['name'])+'\n')
	
for trend in f_trends:
	print trend['name']

	
	https://search.twitter.com/search.json?q=%23SomeoneIWantToMeet
	
	http://search.twitter.com/search.json?q=blue%20angels&rpp=5&include_entities=true&result_type=mixed
	
	https://search.twitter.com/search.json?q=%23SomeoneIWantToMeet
	
	%20 = space
	%23 = #
	
	"""