import urllib
import urllib2
import base64
import httplib
import socket

## authenticate module is used for http basic authentication. found in 'urllib2 missing manual'
def authenticate(username, password):
   password_mgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
   top_level_url = 'http://twitter.com/'
   password_mgr.add_password(None, top_level_url, username, password)
   handler = urllib2.HTTPBasicAuthHandler(password_mgr)
   opener = urllib2.build_opener(handler)

   return opener

## updates twitter status given the status, user name (id or email) and password 
def update_status(status, user, password):   
   data = {'status' : status}   
   data = urllib.urlencode(data)
  
   url = 'http://twitter.com/statuses/update.xml'
  
   opener = authenticate(user, password)
   result = ''
   try:
       handle = opener.open(url, data = data)
       result = handle.read()
       handle.close()   
       return
   except Exception, detail:
       print "Err ", detail

#program starts from here   
update_status('Haani','haanimasood','Prepaid201')